# Trivia
## La meilleure application de Quizz !

Trivia est une application de Quizz en ligne,

- Framework Flutter
- Langage Dart
- Base de données Firebase ✨

## Features

- Authentification : connexion | inscription avec catch des erreurs Firebase
- Page d'accueil pour accéder au jeu
- Page de jeu avec affichage des questions, réponses et vérification
- Page de fin de partie avec envoie du score dans Firebase et synchro du nombre de parties jouées
- Page LeaderBoard pour suivre les scores de tous les joueurs
- Page de profil pour modifier son image et pseudo

## Installation

Pour lancer l'application il vous suffit de lancer un terminal et de taper la commande suivante.

```sh
cd flutter_application_1
flutter pub get
```

Puis vous n'avez plus qu'à lancer le projet.
Il a été développé sur un iPhone SE, donc le design peut être variable selon les appareils utilisés.

Par manque des temps des fonctionnalités sont manquantes, mais l'application peut avoir un bon potentiel !
Merci pour ce projet et pour la découverte de Flutter, ce fût très instructif.