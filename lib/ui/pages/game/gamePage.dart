import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_application_1/data/entities/Question.dart';
import 'package:flutter_application_1/data/repositories/question_repository.dart';
import 'package:flutter_application_1/ui/pages/game/bloc/question_cubit.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../data/repositories/user_repository.dart';
import 'bloc/question_state.dart';

class GamePage extends StatelessWidget {
  GamePage({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController pseudoController = TextEditingController();

  late List<String> answers;
  late String question;
  late String questionCategory;

  QuestionCubit? questionCubit;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<QuestionRepository>(
              create: (_) => QuestionRepository.getInstance(),
            ),
            RepositoryProvider<UserRepository>(
              create: (_) => UserRepository.getInstance(),
            ),
          ],
          child: BlocProvider(
            create: (context) {
              questionCubit = QuestionCubit(
                  gameRepository:
                      RepositoryProvider.of<QuestionRepository>(context),
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context));
              return questionCubit!..getQuestions();
            },
            child: BlocConsumer<QuestionCubit, QuestionState>(
                listener: (context, state) {
              if (state is Error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    )
                    .closed
                    .then((reason) {});
              } else if (state is Saved) {
                // Changement d'écran
              }
              ;
            }, builder: (context, state) {
              if (state is Loading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is Retrieved) {
                return Center(
                    child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(bottom: 30),
                      child: Text("C'est à vous de jouer !",
                          style: TextStyle(fontSize: 20, color: Colors.blue)),
                    ),
                    OutlinedButton(
                        onPressed: () {
                          questionCubit!.getPreparedQuestion();
                        },
                        child: Text("Nouveau quizz !")),
                  ],
                ));
              } else if (state is QuestionReady) {
                answers = state.question.incorrectAnswers!;
                answers.add(state.question.correctAnswer!);
                answers.shuffle();
                question = state.question.question!;
                questionCategory = state.question.category!;
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 70,
                          left: 50,
                          right: 50), //apply padding to all four sides
                      child: Text(
                        questionCategory,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(
                          20), //apply padding to all four sides
                      child: Text(
                        question,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    for (var answer in answers)
                      InkWell(
                        onTap: () {
                          questionCubit!.checkAnswer(answer.toLowerCase());
                        },
                        child: Card(
                            child: Container(
                          margin: EdgeInsets.all(10),
                          child: Text(answer),
                        )),
                      )
                  ],
                );
              } else if (state is CorrectAnswer) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 70,
                          left: 50,
                          right: 50), //apply padding to all four sides
                      child: Text(
                        questionCategory,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(
                          20), //apply padding to all four sides
                      child: Text(
                        question,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    for (var answer in answers)
                      if (state.correctAnswer == answer.toLowerCase())
                        Card(
                            child: Container(
                                color: Colors.green,
                                child: Container(
                                  margin: EdgeInsets.all(10),
                                  child: Text(answer,
                                      style:
                                          const TextStyle(color: Colors.white)),
                                )))
                      else
                        Card(
                            child: Container(
                          margin: EdgeInsets.all(10),
                          child: Text(answer),
                        )),
                    OutlinedButton(
                        onPressed: () {
                          questionCubit!.getPreparedQuestion();
                        },
                        child: Text("Question suivante"))
                  ],
                );
              } else if (state is WrongAnswer) {
                return Column(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(
                          top: 70,
                          left: 50,
                          right: 50), //apply padding to all four sides
                      child: Text(
                        questionCategory,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(
                          20), //apply padding to all four sides
                      child: Text(
                        question,
                        style: const TextStyle(fontWeight: FontWeight.bold),
                        textAlign: TextAlign.center,
                      ),
                    ),
                    for (var answer in answers)
                      if (state.correctAnswer == answer.toLowerCase())
                        Card(
                            child: Container(
                          margin: EdgeInsets.all(10),
                          child: Text(answer,
                              style: const TextStyle(color: Colors.green)),
                        ))
                      else if (state.wrongAnswer == answer.toLowerCase())
                        Card(
                            child: Container(
                          color: Colors.red,
                          child: Container(
                            margin: EdgeInsets.all(10),
                            child: Text(answer,
                                style: const TextStyle(color: Colors.white)),
                          ),
                        ))
                      else
                        Card(
                            child: Container(
                          margin: EdgeInsets.all(10),
                          child: Text(answer),
                        )),
                    OutlinedButton(
                        onPressed: () {
                          questionCubit!.getPreparedQuestion();
                        },
                        child: Text("Question suivante"))
                  ],
                );
              } else if (state is Saved) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text("Partie terminée", style: TextStyle(fontSize: 20)),
                      Container(
                        margin: EdgeInsets.only(top: 16, bottom: 30),
                        child: Text(
                          "Score obtenu : ${state.score}",
                          style: TextStyle(fontSize: 16),
                        ),
                      ),
                      OutlinedButton(
                          onPressed: () {
                            context.beamToNamed('/home');
                          },
                          child: Text("Retour au menu"))
                    ],
                  ),
                );
              } else {
                return OutlinedButton(
                    onPressed: () {
                      context.beamToNamed('/home');
                    },
                    child: Text("Retour au menu"));
              }
            }),
          )),
    );
  }
}
