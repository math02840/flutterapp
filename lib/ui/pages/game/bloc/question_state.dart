import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../data/entities/Question.dart';

part 'question_state.freezed.dart';

@freezed
class QuestionState with _$QuestionState {
  const factory QuestionState.initial() = Initial;
  const factory QuestionState.loading() = Loading;
  const factory QuestionState.retrieved() = Retrieved;
  const factory QuestionState.questionReady(Question question) = QuestionReady;
  const factory QuestionState.correctAnswer(String correctAnswer) = CorrectAnswer;
  const factory QuestionState.wrongAnswer(String wrongAnswer, String correctAnswer) = WrongAnswer;
  const factory QuestionState.saved(int score) = Saved;
  const factory QuestionState.error(String message) = Error;
}
