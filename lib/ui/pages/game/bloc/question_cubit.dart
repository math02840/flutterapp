import 'package:flutter_application_1/data/repositories/question_repository.dart';
import 'package:flutter_application_1/ui/pages/game/bloc/question_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../data/entities/Question.dart';
import '../../../../data/entities/User.dart';
import '../../../../data/repositories/user_repository.dart';

class QuestionCubit extends Cubit<QuestionState> {
  final QuestionRepository gameRepository;
  final UserRepository userRepository;

  int questionNumber = 0;
  late List<Question> questions;
  late String correctAnswer;
  late String questionDifficulty;
  int score = 0;

  QuestionCubit({required this.gameRepository, required this.userRepository})
      : super(const Initial());

  Future<void> getQuestions() async {
    emit(const Loading());
    List<Question> questions = await gameRepository.getQuestionsOfTheDay();
    if (questions != null) {
      this.questions = questions;
      emit(Retrieved());
    } else {
      emit(const Error('Error retriving questions'));
    }
  }

  void getPreparedQuestion() {
    if (questionNumber < 5) {
      emit(const Loading());
      Question question = questions[questionNumber];
      this.correctAnswer = question.correctAnswer!.toLowerCase();
      this.questionDifficulty = question.difficulty!;
      questionNumber += 1;
      emit(QuestionReady(question));
    } else {
      saveGame();
    }
  }

  void checkAnswer(String answer) {
    emit(const Loading());
    if (answer.toLowerCase() == correctAnswer) {
      switch (questionDifficulty) {
        case "easy":
          {
            this.score += 1;
          }
          break;

        case "medium":
          {
            this.score += 2;
          }
          break;

        case "hard":
          {
            this.score += 3;
          }
          break;

        default:
          break;
      }
      emit(CorrectAnswer(correctAnswer));
    } else {
      emit(WrongAnswer(answer, correctAnswer));
    }
  }

  Future<void> saveGame() async {
    emit(const Loading());
    TriviaUser currentUser = await userRepository.getConnectedUser();
    await userRepository.updateUser(
      TriviaUser(
        id: currentUser.id,
        pseudo: currentUser.pseudo,
        avatar: currentUser.avatar,
        score: currentUser.score! + this.score,
        game: currentUser.game! + 1,
      ),
    );
    emit(Saved(score));
  }
}
