// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target

part of 'question_state.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$QuestionState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $QuestionStateCopyWith<$Res> {
  factory $QuestionStateCopyWith(
          QuestionState value, $Res Function(QuestionState) then) =
      _$QuestionStateCopyWithImpl<$Res>;
}

/// @nodoc
class _$QuestionStateCopyWithImpl<$Res>
    implements $QuestionStateCopyWith<$Res> {
  _$QuestionStateCopyWithImpl(this._value, this._then);

  final QuestionState _value;
  // ignore: unused_field
  final $Res Function(QuestionState) _then;
}

/// @nodoc
abstract class _$$InitialCopyWith<$Res> {
  factory _$$InitialCopyWith(_$Initial value, $Res Function(_$Initial) then) =
      __$$InitialCopyWithImpl<$Res>;
}

/// @nodoc
class __$$InitialCopyWithImpl<$Res> extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$InitialCopyWith<$Res> {
  __$$InitialCopyWithImpl(_$Initial _value, $Res Function(_$Initial) _then)
      : super(_value, (v) => _then(v as _$Initial));

  @override
  _$Initial get _value => super._value as _$Initial;
}

/// @nodoc

class _$Initial implements Initial {
  const _$Initial();

  @override
  String toString() {
    return 'QuestionState.initial()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Initial);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return initial();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return initial?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return initial(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return initial?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (initial != null) {
      return initial(this);
    }
    return orElse();
  }
}

abstract class Initial implements QuestionState {
  const factory Initial() = _$Initial;
}

/// @nodoc
abstract class _$$LoadingCopyWith<$Res> {
  factory _$$LoadingCopyWith(_$Loading value, $Res Function(_$Loading) then) =
      __$$LoadingCopyWithImpl<$Res>;
}

/// @nodoc
class __$$LoadingCopyWithImpl<$Res> extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$LoadingCopyWith<$Res> {
  __$$LoadingCopyWithImpl(_$Loading _value, $Res Function(_$Loading) _then)
      : super(_value, (v) => _then(v as _$Loading));

  @override
  _$Loading get _value => super._value as _$Loading;
}

/// @nodoc

class _$Loading implements Loading {
  const _$Loading();

  @override
  String toString() {
    return 'QuestionState.loading()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Loading);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return loading();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return loading?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return loading(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return loading?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (loading != null) {
      return loading(this);
    }
    return orElse();
  }
}

abstract class Loading implements QuestionState {
  const factory Loading() = _$Loading;
}

/// @nodoc
abstract class _$$RetrievedCopyWith<$Res> {
  factory _$$RetrievedCopyWith(
          _$Retrieved value, $Res Function(_$Retrieved) then) =
      __$$RetrievedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RetrievedCopyWithImpl<$Res> extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$RetrievedCopyWith<$Res> {
  __$$RetrievedCopyWithImpl(
      _$Retrieved _value, $Res Function(_$Retrieved) _then)
      : super(_value, (v) => _then(v as _$Retrieved));

  @override
  _$Retrieved get _value => super._value as _$Retrieved;
}

/// @nodoc

class _$Retrieved implements Retrieved {
  const _$Retrieved();

  @override
  String toString() {
    return 'QuestionState.retrieved()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType && other is _$Retrieved);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return retrieved();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return retrieved?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (retrieved != null) {
      return retrieved();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return retrieved(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return retrieved?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (retrieved != null) {
      return retrieved(this);
    }
    return orElse();
  }
}

abstract class Retrieved implements QuestionState {
  const factory Retrieved() = _$Retrieved;
}

/// @nodoc
abstract class _$$QuestionReadyCopyWith<$Res> {
  factory _$$QuestionReadyCopyWith(
          _$QuestionReady value, $Res Function(_$QuestionReady) then) =
      __$$QuestionReadyCopyWithImpl<$Res>;
  $Res call({Question question});
}

/// @nodoc
class __$$QuestionReadyCopyWithImpl<$Res>
    extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$QuestionReadyCopyWith<$Res> {
  __$$QuestionReadyCopyWithImpl(
      _$QuestionReady _value, $Res Function(_$QuestionReady) _then)
      : super(_value, (v) => _then(v as _$QuestionReady));

  @override
  _$QuestionReady get _value => super._value as _$QuestionReady;

  @override
  $Res call({
    Object? question = freezed,
  }) {
    return _then(_$QuestionReady(
      question == freezed
          ? _value.question
          : question // ignore: cast_nullable_to_non_nullable
              as Question,
    ));
  }
}

/// @nodoc

class _$QuestionReady implements QuestionReady {
  const _$QuestionReady(this.question);

  @override
  final Question question;

  @override
  String toString() {
    return 'QuestionState.questionReady(question: $question)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$QuestionReady &&
            const DeepCollectionEquality().equals(other.question, question));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(question));

  @JsonKey(ignore: true)
  @override
  _$$QuestionReadyCopyWith<_$QuestionReady> get copyWith =>
      __$$QuestionReadyCopyWithImpl<_$QuestionReady>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return questionReady(question);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return questionReady?.call(question);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (questionReady != null) {
      return questionReady(question);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return questionReady(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return questionReady?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (questionReady != null) {
      return questionReady(this);
    }
    return orElse();
  }
}

abstract class QuestionReady implements QuestionState {
  const factory QuestionReady(final Question question) = _$QuestionReady;

  Question get question => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$QuestionReadyCopyWith<_$QuestionReady> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$CorrectAnswerCopyWith<$Res> {
  factory _$$CorrectAnswerCopyWith(
          _$CorrectAnswer value, $Res Function(_$CorrectAnswer) then) =
      __$$CorrectAnswerCopyWithImpl<$Res>;
  $Res call({String correctAnswer});
}

/// @nodoc
class __$$CorrectAnswerCopyWithImpl<$Res>
    extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$CorrectAnswerCopyWith<$Res> {
  __$$CorrectAnswerCopyWithImpl(
      _$CorrectAnswer _value, $Res Function(_$CorrectAnswer) _then)
      : super(_value, (v) => _then(v as _$CorrectAnswer));

  @override
  _$CorrectAnswer get _value => super._value as _$CorrectAnswer;

  @override
  $Res call({
    Object? correctAnswer = freezed,
  }) {
    return _then(_$CorrectAnswer(
      correctAnswer == freezed
          ? _value.correctAnswer
          : correctAnswer // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$CorrectAnswer implements CorrectAnswer {
  const _$CorrectAnswer(this.correctAnswer);

  @override
  final String correctAnswer;

  @override
  String toString() {
    return 'QuestionState.correctAnswer(correctAnswer: $correctAnswer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$CorrectAnswer &&
            const DeepCollectionEquality()
                .equals(other.correctAnswer, correctAnswer));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType, const DeepCollectionEquality().hash(correctAnswer));

  @JsonKey(ignore: true)
  @override
  _$$CorrectAnswerCopyWith<_$CorrectAnswer> get copyWith =>
      __$$CorrectAnswerCopyWithImpl<_$CorrectAnswer>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return correctAnswer(this.correctAnswer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return correctAnswer?.call(this.correctAnswer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (correctAnswer != null) {
      return correctAnswer(this.correctAnswer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return correctAnswer(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return correctAnswer?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (correctAnswer != null) {
      return correctAnswer(this);
    }
    return orElse();
  }
}

abstract class CorrectAnswer implements QuestionState {
  const factory CorrectAnswer(final String correctAnswer) = _$CorrectAnswer;

  String get correctAnswer => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$CorrectAnswerCopyWith<_$CorrectAnswer> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$WrongAnswerCopyWith<$Res> {
  factory _$$WrongAnswerCopyWith(
          _$WrongAnswer value, $Res Function(_$WrongAnswer) then) =
      __$$WrongAnswerCopyWithImpl<$Res>;
  $Res call({String wrongAnswer, String correctAnswer});
}

/// @nodoc
class __$$WrongAnswerCopyWithImpl<$Res>
    extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$WrongAnswerCopyWith<$Res> {
  __$$WrongAnswerCopyWithImpl(
      _$WrongAnswer _value, $Res Function(_$WrongAnswer) _then)
      : super(_value, (v) => _then(v as _$WrongAnswer));

  @override
  _$WrongAnswer get _value => super._value as _$WrongAnswer;

  @override
  $Res call({
    Object? wrongAnswer = freezed,
    Object? correctAnswer = freezed,
  }) {
    return _then(_$WrongAnswer(
      wrongAnswer == freezed
          ? _value.wrongAnswer
          : wrongAnswer // ignore: cast_nullable_to_non_nullable
              as String,
      correctAnswer == freezed
          ? _value.correctAnswer
          : correctAnswer // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$WrongAnswer implements WrongAnswer {
  const _$WrongAnswer(this.wrongAnswer, this.correctAnswer);

  @override
  final String wrongAnswer;
  @override
  final String correctAnswer;

  @override
  String toString() {
    return 'QuestionState.wrongAnswer(wrongAnswer: $wrongAnswer, correctAnswer: $correctAnswer)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$WrongAnswer &&
            const DeepCollectionEquality()
                .equals(other.wrongAnswer, wrongAnswer) &&
            const DeepCollectionEquality()
                .equals(other.correctAnswer, correctAnswer));
  }

  @override
  int get hashCode => Object.hash(
      runtimeType,
      const DeepCollectionEquality().hash(wrongAnswer),
      const DeepCollectionEquality().hash(correctAnswer));

  @JsonKey(ignore: true)
  @override
  _$$WrongAnswerCopyWith<_$WrongAnswer> get copyWith =>
      __$$WrongAnswerCopyWithImpl<_$WrongAnswer>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return wrongAnswer(this.wrongAnswer, this.correctAnswer);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return wrongAnswer?.call(this.wrongAnswer, this.correctAnswer);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (wrongAnswer != null) {
      return wrongAnswer(this.wrongAnswer, this.correctAnswer);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return wrongAnswer(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return wrongAnswer?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (wrongAnswer != null) {
      return wrongAnswer(this);
    }
    return orElse();
  }
}

abstract class WrongAnswer implements QuestionState {
  const factory WrongAnswer(
      final String wrongAnswer, final String correctAnswer) = _$WrongAnswer;

  String get wrongAnswer => throw _privateConstructorUsedError;
  String get correctAnswer => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$WrongAnswerCopyWith<_$WrongAnswer> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$SavedCopyWith<$Res> {
  factory _$$SavedCopyWith(_$Saved value, $Res Function(_$Saved) then) =
      __$$SavedCopyWithImpl<$Res>;
  $Res call({int score});
}

/// @nodoc
class __$$SavedCopyWithImpl<$Res> extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$SavedCopyWith<$Res> {
  __$$SavedCopyWithImpl(_$Saved _value, $Res Function(_$Saved) _then)
      : super(_value, (v) => _then(v as _$Saved));

  @override
  _$Saved get _value => super._value as _$Saved;

  @override
  $Res call({
    Object? score = freezed,
  }) {
    return _then(_$Saved(
      score == freezed
          ? _value.score
          : score // ignore: cast_nullable_to_non_nullable
              as int,
    ));
  }
}

/// @nodoc

class _$Saved implements Saved {
  const _$Saved(this.score);

  @override
  final int score;

  @override
  String toString() {
    return 'QuestionState.saved(score: $score)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Saved &&
            const DeepCollectionEquality().equals(other.score, score));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(score));

  @JsonKey(ignore: true)
  @override
  _$$SavedCopyWith<_$Saved> get copyWith =>
      __$$SavedCopyWithImpl<_$Saved>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return saved(score);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return saved?.call(score);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (saved != null) {
      return saved(score);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return saved(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return saved?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (saved != null) {
      return saved(this);
    }
    return orElse();
  }
}

abstract class Saved implements QuestionState {
  const factory Saved(final int score) = _$Saved;

  int get score => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$SavedCopyWith<_$Saved> get copyWith => throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$ErrorCopyWith<$Res> {
  factory _$$ErrorCopyWith(_$Error value, $Res Function(_$Error) then) =
      __$$ErrorCopyWithImpl<$Res>;
  $Res call({String message});
}

/// @nodoc
class __$$ErrorCopyWithImpl<$Res> extends _$QuestionStateCopyWithImpl<$Res>
    implements _$$ErrorCopyWith<$Res> {
  __$$ErrorCopyWithImpl(_$Error _value, $Res Function(_$Error) _then)
      : super(_value, (v) => _then(v as _$Error));

  @override
  _$Error get _value => super._value as _$Error;

  @override
  $Res call({
    Object? message = freezed,
  }) {
    return _then(_$Error(
      message == freezed
          ? _value.message
          : message // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$Error implements Error {
  const _$Error(this.message);

  @override
  final String message;

  @override
  String toString() {
    return 'QuestionState.error(message: $message)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$Error &&
            const DeepCollectionEquality().equals(other.message, message));
  }

  @override
  int get hashCode =>
      Object.hash(runtimeType, const DeepCollectionEquality().hash(message));

  @JsonKey(ignore: true)
  @override
  _$$ErrorCopyWith<_$Error> get copyWith =>
      __$$ErrorCopyWithImpl<_$Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() initial,
    required TResult Function() loading,
    required TResult Function() retrieved,
    required TResult Function(Question question) questionReady,
    required TResult Function(String correctAnswer) correctAnswer,
    required TResult Function(String wrongAnswer, String correctAnswer)
        wrongAnswer,
    required TResult Function(int score) saved,
    required TResult Function(String message) error,
  }) {
    return error(message);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
  }) {
    return error?.call(message);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? initial,
    TResult Function()? loading,
    TResult Function()? retrieved,
    TResult Function(Question question)? questionReady,
    TResult Function(String correctAnswer)? correctAnswer,
    TResult Function(String wrongAnswer, String correctAnswer)? wrongAnswer,
    TResult Function(int score)? saved,
    TResult Function(String message)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(message);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(Initial value) initial,
    required TResult Function(Loading value) loading,
    required TResult Function(Retrieved value) retrieved,
    required TResult Function(QuestionReady value) questionReady,
    required TResult Function(CorrectAnswer value) correctAnswer,
    required TResult Function(WrongAnswer value) wrongAnswer,
    required TResult Function(Saved value) saved,
    required TResult Function(Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(Initial value)? initial,
    TResult Function(Loading value)? loading,
    TResult Function(Retrieved value)? retrieved,
    TResult Function(QuestionReady value)? questionReady,
    TResult Function(CorrectAnswer value)? correctAnswer,
    TResult Function(WrongAnswer value)? wrongAnswer,
    TResult Function(Saved value)? saved,
    TResult Function(Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class Error implements QuestionState {
  const factory Error(final String message) = _$Error;

  String get message => throw _privateConstructorUsedError;
  @JsonKey(ignore: true)
  _$$ErrorCopyWith<_$Error> get copyWith => throw _privateConstructorUsedError;
}
