import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:uuid/uuid.dart';

import '../../../../data/entities/User.dart';
import '../../../../data/repositories/auth_repository.dart';
import '../../../../data/repositories/user_repository.dart';
import 'bloc/signin_cubit.dart';
import 'bloc/signin_state.dart';

class SignIn extends StatelessWidget {
  SignIn({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController pseudoController = TextEditingController();
  var uuid = const Uuid();

  SignInCubit? signInCubit;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<AuthRepository>(
              create: (_) => AuthRepository.getInstance(),
            ),
            RepositoryProvider<UserRepository>(
              create: (_) => UserRepository.getInstance(),
            ),
          ],
          child: BlocProvider(
            create: (context) {
              signInCubit = SignInCubit(
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context),
                  authRepository:
                      RepositoryProvider.of<AuthRepository>(context));
              return signInCubit!..isUserAuthenticated();
            },
            child: BlocConsumer<SignInCubit, SignInState>(
                listener: (context, state) {
              if (state is Error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    )
                    .closed
                    .then((reason) {});
              } else if (state is Retrieved) {
                context.beamToNamed('/home');
              }
              ;
            }, builder: (context, state) {
              if (state is Loading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 100),
                      child: Text(
                        "Trivia",
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 40),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 50, left: 20, right: 20),
                      child: TextField(
                        controller: emailController,
                        decoration: const InputDecoration(
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: TextField(
                        controller: passwordController,
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        decoration: const InputDecoration(
                          labelText: 'Mot de passe',
                        ),
                      ),
                    ),
                    OutlinedButton(
                      child: const Text('Se connecter'),
                      onPressed: () {
                        signInCubit!.signIn(
                            emailController.text, passwordController.text);
                        emailController.text = '';
                        passwordController.text = '';
                      },
                    ),
                    Container(
                      child: Text('Pas de compte ?'),
                      margin: EdgeInsets.only(top: 20),
                    ),
                    Container(
                      child: InkWell(
                        child: Text(
                          'S\'inscrire',
                          style: TextStyle(color: Colors.blue),
                        ),
                        onTap: () {
                          context.beamToNamed('/signup');
                        },
                      ),
                    ),
                  ],
                );
              }
            }),
          )),
    );
  }
}
