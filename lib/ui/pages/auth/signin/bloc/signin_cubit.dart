import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_application_1/ui/pages/auth/signin/bloc/signin_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../../../../../data/repositories/auth_repository.dart';
import '../../../../../data/repositories/user_repository.dart';

class SignInCubit extends Cubit<SignInState> {
  final AuthRepository authRepository;
  final UserRepository userRepository;

  SignInCubit({required this.authRepository, required this.userRepository})
      : super(const Initial());

  Future<void> signIn(String email, String password) async {
    emit(const Loading());
    if (email.isNotEmpty && password.isNotEmpty) {
      try {
        await authRepository.signIn(email: email, password: password);
        User? isUserAuthenticated = await authRepository.getAuthenticatedUser();
        if (isUserAuthenticated != null) {
          await userRepository.getUserById(isUserAuthenticated.uid);
          emit(const Retrieved());
        } else {
          emit(const Error('Une erreur est survenue'));
        }
      } on FirebaseAuthException catch (e) {
        emit(Error('${e.message}'));
      }
    } else {
      emit(const Error('Veuillez remplir tous les champs'));
    }
  }

  Future<void> isUserAuthenticated() async {
    emit(Loading());
    User? isUserAuthenticated = await authRepository.getAuthenticatedUser();
    if (isUserAuthenticated != null) {
      await userRepository.getUserById(isUserAuthenticated.uid);
      emit(const Retrieved());
    } else {
      emit(Initial());
    }
  }
}
