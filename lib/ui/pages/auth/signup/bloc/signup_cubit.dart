import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_application_1/ui/pages/auth/signup/bloc/signup_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../../data/entities/User.dart';
import '../../../../../data/repositories/auth_repository.dart';
import '../../../../../data/repositories/user_repository.dart';

class SignUpCubit extends Cubit<SignUpState> {
  final AuthRepository authRepository;
  final UserRepository userRepository;

  SignUpCubit({required this.authRepository, required this.userRepository})
      : super(const Initial());

  Future<void> registerUser(
      String email, String password, TriviaUser user, XFile file) async {
    emit(const Loading());
    if (email.isNotEmpty && password.isNotEmpty && user.pseudo != "") {
      try {
        User? userFromFirebase =
            await authRepository.signUp(email: email, password: password);
        if (userFromFirebase != null) {
          await userRepository.createUser(user, userFromFirebase.uid);
          await userRepository.uploadAvatar(file);
          User? isUserAuthenticated = await authRepository.getAuthenticatedUser();
        if (isUserAuthenticated != null) {
          await userRepository.getUserById(isUserAuthenticated.uid);
          emit(const Saved());
        } else {
          emit(const Error('Une erreur est survenue'));
        }
          
        } else {
          emit(const Error('Error signing up'));
        }
      } on FirebaseAuthException catch (e) {
        emit(Error('${e.message}'));
      }
    } else {
      emit(const Error('Veuillez remplir tous les champs'));
    }
  }
}
