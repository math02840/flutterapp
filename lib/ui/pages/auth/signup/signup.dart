import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import 'package:uuid/uuid.dart';

import '../../../../data/entities/User.dart';
import '../../../../data/repositories/auth_repository.dart';
import '../../../../data/repositories/user_repository.dart';
import 'bloc/signup_cubit.dart';
import 'bloc/signup_state.dart';

class SignUp extends StatelessWidget {
  SignUp({Key? key}) : super(key: key);

  final TextEditingController emailController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();
  final TextEditingController pseudoController = TextEditingController();
  var uuid = const Uuid();

  SignUpCubit? signUpCubit;

  @override
  Widget build(BuildContext context) {
    final ImagePicker _picker = ImagePicker();

    XFile? image;

    onTapPicture() async {
      image = await _picker.pickImage(source: ImageSource.gallery);
    }

    return Scaffold(
      body: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<AuthRepository>(
              create: (_) => AuthRepository.getInstance(),
            ),
            RepositoryProvider<UserRepository>(
              create: (_) => UserRepository.getInstance(),
            ),
          ],
          child: BlocProvider(
            create: (context) {
              signUpCubit = SignUpCubit(
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context),
                  authRepository:
                      RepositoryProvider.of<AuthRepository>(context));
              return signUpCubit!;
            },
            child: BlocConsumer<SignUpCubit, SignUpState>(
                listener: (context, state) {
              if (state is Error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    )
                    .closed
                    .then((reason) {});
              } else if (state is Saved) {
                context.beamToNamed('/home');
              }
              ;
            }, builder: (context, state) {
              if (state is Loading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else {
                return Column(
                  children: [
                    Container(
                      margin: EdgeInsets.only(top: 100),
                      child: Text(
                        "Trivia",
                        style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                            fontSize: 40),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(top: 30, left: 20, right: 20),
                      child: TextField(
                        controller: emailController,
                        decoration: InputDecoration(
                          labelText: 'Email',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.all(20),
                      child: TextField(
                        controller: passwordController,
                        obscureText: true,
                        enableSuggestions: false,
                        autocorrect: false,
                        decoration: InputDecoration(
                          labelText: 'Mot de passe',
                        ),
                      ),
                    ),
                    Container(
                      margin: EdgeInsets.only(left: 20, right: 20, bottom: 20),
                      child: TextField(
                        controller: pseudoController,
                        decoration: const InputDecoration(
                          labelText: 'Pseudo',
                        ),
                      ),
                    ),
                    OutlinedButton(
                      child: Text('Choisir une image'),
                      onPressed: () {
                        onTapPicture();
                      },
                    ),
                    OutlinedButton(
                      child: Text('S\'inscrire'),
                      onPressed: () {
                        if (image != null) {
                          signUpCubit!.registerUser(
                            emailController.text,
                            passwordController.text,
                            TriviaUser(
                                id: uuid.v4(),
                                score: 0,
                                game: 0,
                                pseudo: pseudoController.text,
                                avatar: ''),
                            image!,
                          );
                          emailController.text = '';
                          passwordController.text = '';
                          pseudoController.text = '';
                        } else {
                          ScaffoldMessenger.of(context)
                              .showSnackBar(
                                SnackBar(
                                  content:
                                      Text("Veuillez sélectionner une image"),
                                ),
                              )
                              .closed
                              .then((reason) {});
                        }
                      },
                    ),
                    Container(
                      child: Text('Déjà un compte ?'),
                      margin: EdgeInsets.only(top: 20),
                    ),
                    Container(
                      child: InkWell(
                        child: Text(
                          'Se connnecter',
                          style: TextStyle(color: Colors.blue),
                        ),
                        onTap: () {
                          context.beamToNamed('/');
                        },
                      ),
                    ),
                  ],
                );
              }
            }),
          )),
    );
  }
}
