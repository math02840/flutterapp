import 'dart:io';

import 'package:beamer/beamer.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:image_picker/image_picker.dart';

import '../../../data/entities/User.dart';
import '../../../data/repositories/auth_repository.dart';
import '../../../data/repositories/user_repository.dart';
import 'bloc/profil_cubit.dart';
import 'bloc/profil_state.dart';

class ProfilePage extends StatelessWidget {
  ProfilePage({Key? key}) : super(key: key);

  ProfileCubit? profileCubit;
  final TextEditingController pseudoController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final ImagePicker _picker = ImagePicker();

    XFile? image;

    return Scaffold(
      body: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<AuthRepository>(
              create: (_) => AuthRepository.getInstance(),
            ),
            RepositoryProvider<UserRepository>(
              create: (_) => UserRepository.getInstance(),
            ),
          ],
          child: BlocProvider(
            create: (context) {
              profileCubit = ProfileCubit(
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context),
                  authRepository:
                      RepositoryProvider.of<AuthRepository>(context));
              return profileCubit!..getUserData();
            },
            child: BlocConsumer<ProfileCubit, ProfilState>(
                listener: (context, state) {
              if (state is Error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    )
                    .closed
                    .then((reason) {});
              } else if (state is SignedOut) {
                context.beamToNamed('/');
              }
              ;
            }, builder: (context, state) {
              if (state is Loading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is Retrieved) {
                var showImage;
                image != null
                    ? showImage = FileImage(File(image!.path))
                    : showImage = NetworkImage(state.imgUrl);
                return Container(
                    margin: const EdgeInsets.all(20),
                    child: Column(children: <Widget>[
                      Container(
                        width: 200,
                        height: 200,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              width: 200,
                              height: 200,
                              child: CircleAvatar(
                                backgroundImage: showImage,
                              ),
                            ),
                            Align(
                                alignment: Alignment.topRight,
                                child: Container(
                                    margin: const EdgeInsets.only(
                                        right: 10, top: 10),
                                    child: InkWell(
                                      onTap: () async {
                                        image = await _picker.pickImage(
                                            source: ImageSource.gallery);
                                        profileCubit!.updateUserImg(image!);
                                      },
                                      child: SvgPicture.asset(
                                        'assets/icons/editImage.svg',
                                        color: const Color.fromARGB(
                                            255, 26, 118, 216),
                                        width: 30,
                                      ),
                                    ))),
                          ],
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(state.user.pseudo!,
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 26)),
                            Container(
                                margin: const EdgeInsets.only(left: 10),
                                child: InkWell(
                                    onTap: () {
                                      showModalBottomSheet<void>(
                                        context: context,
                                        builder: (BuildContext context) {
                                          return Container(
                                            margin: const EdgeInsets.all(20),
                                            color: Colors.white,
                                            child: Center(
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  TextField(
                                                    controller:
                                                        pseudoController,
                                                    decoration:
                                                        const InputDecoration(
                                                            border: InputBorder
                                                                .none,
                                                            labelText:
                                                                'Entrez votre nom'),
                                                  ),
                                                  ElevatedButton(
                                                      child: const Text(
                                                          'Sauvegarder'),
                                                      onPressed: () {
                                                        profileCubit!.updateUserPseudo(
                                                            TriviaUser(
                                                                id: state
                                                                    .user.id,
                                                                score: state
                                                                    .user.score,
                                                                game: state
                                                                    .user.game,
                                                                pseudo:
                                                                    pseudoController
                                                                        .text,
                                                                avatar: state
                                                                    .user
                                                                    .avatar));
                                                        Navigator.pop(context);
                                                      })
                                                ],
                                              ),
                                            ),
                                          );
                                        },
                                      );
                                    },
                                    child: SvgPicture.asset(
                                        'assets/icons/pencil.svg',
                                        height: 24,
                                        width: 24,
                                        color: const Color.fromARGB(
                                            255, 26, 118, 216))))
                          ],
                        ),
                      ),
                      Card(
                        margin: const EdgeInsets.only(top: 30),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(15.0),
                        ),
                        child: Container(
                          margin: const EdgeInsets.only(
                              top: 20, left: 10, right: 10),
                          child: Column(children: <Widget>[
                            Row(children: <Widget>[
                              Container(
                                  margin: const EdgeInsets.only(left: 10),
                                  child: Image.asset(
                                    'assets/img/trophy.png',
                                    height: 30,
                                    width: 30,
                                  )),
                              Container(
                                  margin: const EdgeInsets.only(left: 30),
                                  child: Text(
                                      "Votre score global est de ${state.user.score}!",
                                      style: const TextStyle(
                                          color: Color.fromARGB(
                                              255, 110, 110, 110),
                                          fontSize: 14))),
                            ]),
                            Container(
                                margin: const EdgeInsets.all(20),
                                child: Text(
                                    "Félicitations, vous avez joué ${state.user.game} parties !",
                                    style: const TextStyle(
                                        color:
                                            Color.fromARGB(255, 110, 110, 110),
                                        fontSize: 14)))
                          ]),
                        ),
                      ),
                      Container(
                        margin: const EdgeInsets.only(top: 30),
                        child: OutlinedButton(
                          child: Text(
                            "Se déconnecter",
                            style: TextStyle(color: Colors.red),
                          ),
                          onPressed: () {
                            profileCubit!.signOut();
                          },
                          style: OutlinedButton.styleFrom(
                            side: BorderSide(width: 1.0, color: Colors.red),
                          ),
                        ),
                      )
                    ]));
              } else {
                return const Text(
                  'Erreur',
                  style: TextStyle(color: Colors.red),
                );
              }
            }),
          )),
    );
  }
}
