import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../data/entities/User.dart';

part 'profil_state.freezed.dart';

@freezed
class ProfilState with _$ProfilState {
  const factory ProfilState.initial() = Initial;
  const factory ProfilState.loading() = Loading;
  const factory ProfilState.retrieved(TriviaUser user, String imgUrl) =
      Retrieved;
  const factory ProfilState.signedOut() = SignedOut;
  const factory ProfilState.error(String message) = Error;
}
