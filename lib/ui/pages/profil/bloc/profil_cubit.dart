import 'package:flutter_application_1/data/entities/User.dart';
import 'package:flutter_application_1/data/repositories/user_repository.dart';
import 'package:flutter_application_1/ui/pages/profil/bloc/profil_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:image_picker/image_picker.dart';

import '../../../../data/repositories/auth_repository.dart';

class ProfileCubit extends Cubit<ProfilState> {
  final UserRepository userRepository;
  final AuthRepository authRepository;

  ProfileCubit({required this.userRepository, required this.authRepository}) : super(const Initial());

  Future<void> getUserData() async {
    emit(const Loading());
    TriviaUser? user = await userRepository.getConnectedUser();
    String imageUrl = await userRepository.retrieveImage();
    if (user != null) {
      emit(Retrieved(user, imageUrl));
    } else {
      emit(const Error('Error retriving user data'));
    }
  }

  Future<void> updateUserPseudo(TriviaUser newUser) async {
    emit(const Loading());
    await userRepository.updateUser(newUser);
    String imageUrl = await userRepository.retrieveImage();
    TriviaUser? user = await userRepository.getConnectedUser();
    if (user != null) {
      emit(Retrieved(user, imageUrl));
    } else {
      emit(const Error('Error retriving user data'));
    }
  }

  Future<void> updateUserImg(XFile file) async {
    emit(const Loading());
    await userRepository.uploadAvatar(file);
    String imageUrl = await userRepository.retrieveImage();
    TriviaUser? user = await userRepository.getConnectedUser();
    if (user != null) {
      emit(Retrieved(user, imageUrl));
    } else {
      emit(const Error('Error retriving user data'));
    }
  }

  Future<void> signOut() async {
    emit(Loading());
    await authRepository.signOut();
    userRepository.signOut();
    emit(const SignedOut());
  }
}
