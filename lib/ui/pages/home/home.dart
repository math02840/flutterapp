import 'package:beamer/beamer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

class Home extends StatelessWidget {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        margin: const EdgeInsets.all(20),
        child: SingleChildScrollView(
          child: Column(children: <Widget>[
            Column(
              children: <Widget>[
                Container(
                    margin: EdgeInsets.only(
                      top: 20,
                    ),
                    width: 300,
                    height: 300,
                    child: Image.asset('assets/img/Icons_026.png')),
                
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 50, vertical: 10),
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20))),
                  child: const Text(
                    'Jouer',
                    style: TextStyle(fontSize: 24),
                  ),
                  onPressed: () {
                    context.beamToNamed('/game');
                  },
                ),
              ],
            ),
            Card(
                margin: const EdgeInsets.only(top: 30),
                color: Colors.white,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                child: Container(
                    margin: const EdgeInsets.only(top: 20, left: 10, right: 10),
                    child: Column(children: <Widget>[
                      Row(children: <Widget>[
                        Container(
                            margin: const EdgeInsets.only(left: 10),
                            child: Image.asset(
                              'assets/img/Icons_026.png',
                              height: 50,
                              width: 50,
                            )),
                        Container(
                            margin: const EdgeInsets.only(left: 30),
                            child: Text("Rêgles du jeu",
                                style: const TextStyle(
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromARGB(255, 110, 110, 110),
                                    fontSize: 14))),
                      ]),
                      Container(
                          margin: const EdgeInsets.all(20),
                          child: Text(
                              "Répondez aux bonnes réponses pour gagner des points. Objectif, être le/la meilleur(e) des meilleur(e)s.",
                              style: const TextStyle(
                                  color: Color.fromARGB(255, 110, 110, 110),
                                  fontSize: 14)))
                    ]))),
          ]),
        ));
  }
}
