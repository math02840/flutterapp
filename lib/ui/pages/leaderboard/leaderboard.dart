import 'package:flutter/material.dart';
import 'package:flutter_application_1/data/entities/Question.dart';
import 'package:flutter_application_1/data/repositories/question_repository.dart';
import 'package:flutter_application_1/ui/pages/game/bloc/question_cubit.dart';
import 'package:flutter_application_1/ui/pages/leaderboard/bloc/leaderboard_cubit.dart';
import 'package:flutter_application_1/ui/pages/leaderboard/bloc/leaderboard_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';

import '../../../data/repositories/user_repository.dart';

class LeaderboardPage extends StatelessWidget {
  LeaderboardPage({Key? key}) : super(key: key);

  LeaderboardCubit? leaderboardCubit;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: MultiRepositoryProvider(
          providers: [
            RepositoryProvider<UserRepository>(
              create: (_) => UserRepository.getInstance(),
            ),
          ],
          child: BlocProvider(
            create: (context) {
              leaderboardCubit = LeaderboardCubit(
                  userRepository:
                      RepositoryProvider.of<UserRepository>(context));
              return leaderboardCubit!..getUsersData();
            },
            child: BlocConsumer<LeaderboardCubit, LeaderboardState>(
                listener: (context, state) {
              if (state is Error) {
                ScaffoldMessenger.of(context)
                    .showSnackBar(
                      SnackBar(
                        content: Text(state.message),
                      ),
                    )
                    .closed
                    .then((reason) {});
              }
              ;
            }, builder: (context, state) {
              if (state is Loading) {
                return const Center(
                  child: CircularProgressIndicator(),
                );
              } else if (state is Retrieved) {
                return ListView.separated(
                    itemBuilder: (BuildContext context, int index) {
                      var medaille;

                      switch (index) {
                        case 0:
                          {
                            medaille = SvgPicture.asset(
                              'assets/icons/medal.svg',
                              color: Colors.yellow,
                            );
                          }
                          break;

                        case 1:
                          {
                            medaille = SvgPicture.asset(
                                'assets/icons/medal.svg',
                                color: Colors.grey);
                          }
                          break;

                        case 2:
                          {
                            medaille = SvgPicture.asset(
                              'assets/icons/medal.svg',
                              color: Colors.orange,
                            );
                          }
                          break;
                      }

                      return ListTile(
                        contentPadding: const EdgeInsets.all(15),
                        leading: Text("${index + 1}"),
                        title: Text(
                            '${state.users[index].pseudo!} | Score : ${state.users[index].score}'),
                        trailing: medaille,
                        tileColor: index % 2 == 0
                            ? Color.fromARGB(255, 199, 199, 199)
                            : null,
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return const Divider(
                        color: Colors.grey,
                        height: 1,
                      );
                    },
                    itemCount: state.users.length);
              } else {
                return const Text('Pas de questions');
              }
            }),
          )),
    );
  }
}
