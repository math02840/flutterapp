import 'package:flutter_application_1/data/entities/User.dart';
import 'package:flutter_application_1/data/repositories/user_repository.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import 'leaderboard_state.dart';

class LeaderboardCubit extends Cubit<LeaderboardState> {
  final UserRepository userRepository;

  LeaderboardCubit({required this.userRepository}) : super(const Initial());

  Future<void> getUsersData() async {
    emit(const Loading());
    List<TriviaUser>? users = await userRepository.getAllUsers();
    if (users != null) {
      users.sort((b, a) => a.score!.compareTo(b.score!));
      emit(Retrieved(users));
    } else {
      emit(const Error('Error retriving users data'));
    }
  }
}
