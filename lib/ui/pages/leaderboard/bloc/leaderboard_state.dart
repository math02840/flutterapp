import 'package:freezed_annotation/freezed_annotation.dart';

import '../../../../data/entities/User.dart';

part 'leaderboard_state.freezed.dart';

@freezed
class LeaderboardState with _$LeaderboardState {
  const factory LeaderboardState.initial() = Initial;
  const factory LeaderboardState.loading() = Loading;
  const factory LeaderboardState.retrieved(List<TriviaUser> users) = Retrieved;
  const factory LeaderboardState.error(String message) = Error;
}
