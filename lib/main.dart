import 'package:beamer/beamer.dart';
import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_application_1/ui/pages/auth/signin/signin.dart';
import 'package:flutter_application_1/ui/pages/auth/signup/signup.dart';
import 'firebase_options.dart';
import 'ui/pages/Nav/navbar.dart';
import 'ui/pages/game/gamePage.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);

  final routerDelegate = BeamerDelegate(
      locationBuilder: RoutesLocationBuilder(routes: {
    '/': (context, state, data) => SignIn(),
    '/signup': (context, state, data) => SignUp(),
    '/game': (context, state, data) => GamePage(),
    '/home': (context, state, data) => const Navbar(),
  }));

  @override
  Widget build(BuildContext context) {
    return MaterialApp.router(
      routeInformationParser: BeamerParser(),
      routerDelegate: routerDelegate,
      backButtonDispatcher:
          BeamerBackButtonDispatcher(delegate: routerDelegate),
    );
  }
}
