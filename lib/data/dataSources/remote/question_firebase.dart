import 'package:cloud_firestore/cloud_firestore.dart';

import '../../entities/Question.dart';

class QuestionsFirebase {
  static final FirebaseFirestore _firebaseFirestore =
      FirebaseFirestore.instance;
  static QuestionsFirebase? _instance;

  static late final CollectionReference<Question> _questionRef;

  QuestionsFirebase._();

  static getInstance() {
    if (_instance == null) {
      _questionRef = _firebaseFirestore.collection('questions').withConverter(
          fromFirestore: ((snapshot, _) => Question.fromJson(snapshot.data()!)),
          toFirestore: ((question, _) => question.toJson()));
    }

    _instance = QuestionsFirebase._();
    return _instance;
  }

  Future<DocumentReference<Question>> insertQuestion(Question question) async =>
      _questionRef.add(question);

  Future<QuerySnapshot<Question>> getQuestions() async =>
      await _questionRef.get();

  Future<QuerySnapshot<Question>> getByType(String type) async =>
      await _questionRef.where('type', isEqualTo: type).get();

  Future<void> deleteQuestion() async {
    QuerySnapshot<Question> questions = await _questionRef.get();
    await _questionRef.doc(questions.docs.first.id).delete();
  }
}
