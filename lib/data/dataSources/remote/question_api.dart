import 'dart:convert';

import '../../entities/Question.dart';
import '../../entities/Results.dart';

import 'package:http/http.dart' as http;

class QuestionApi {
  final String _baseUrl = "opentdb.com";

  static QuestionApi? _instance;

  static getInstance() {
    _instance ??= QuestionApi._();
    return _instance;
  }

  QuestionApi._();

  Future<List<Question>> getQuestionsOfTheDay() async {
    final queryParameters = {'amount': '5'};
    final uri = Uri.https(_baseUrl, '/api.php', queryParameters);

    final response = await http.get(uri);
    if (response.statusCode == 200) {
      Results questionApiResponse = Results.fromJson(jsonDecode(response.body));
      return questionApiResponse.results!;
    } else {
      throw Exception('Failed to load questions');
    }
  }
}
