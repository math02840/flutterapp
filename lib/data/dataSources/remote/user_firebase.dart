import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter_application_1/data/dataSources/remote/auth_firebase.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io' as io;

import '../../entities/User.dart';

class UserFirebase {
  static final FirebaseFirestore _firebaseFirestore =
      FirebaseFirestore.instance;

  static final FirebaseStorage _firebaseStorage = FirebaseStorage.instance;

  static final AuthFirebase _userFirestore = AuthFirebase.getInstance();

  static UserFirebase? _instance;

  static late final CollectionReference<TriviaUser> _userRef;

  UserFirebase._();

  static UserFirebase getInstance() {
    if (_instance == null) {
      _userRef =
          _firebaseFirestore.collection('users').withConverter<TriviaUser>(
                fromFirestore: (snapshot, _) =>
                    TriviaUser.fromJson(snapshot.data()!),
                toFirestore: (user, _) => user.toJson(),
              );
      _instance = UserFirebase._();
    }
    return _instance!;
  }

  Future<DocumentReference<TriviaUser>> createUser(TriviaUser user) async =>
      _userRef.add(user);

  Future<List<TriviaUser>> getUsers() async {
    final users = await _userRef.get();
    return users.docs.map((e) => e.data()).toList();
  }

  Future<TriviaUser?> getUserById(String id) async {
    var document = await _userRef.doc(id).get();
    return document.data();
  }

  Future<void> insertUserWithId(TriviaUser user, String id) async =>
      _userRef.doc(id).set(user);

  Future<void> updateUser(TriviaUser user) async =>
      _userRef.doc(_userFirestore.getUserId()).update(user.toJson());

  Future<void> deleteUser(TriviaUser user) async =>
      _userRef.doc(user.id.toString()).delete();

  Future<UploadTask?> uploadFile(XFile file) async {
    UploadTask uploadTask;

    Reference ref = _firebaseStorage.ref().child('${_userFirestore.getUserId()}.jpg');

    final metadata = SettableMetadata(contentType: 'image/jpeg');

    if (kIsWeb) {
      uploadTask = ref.putData(await file.readAsBytes(), metadata);
    } else {
      uploadTask = ref.putFile(io.File(file.path), metadata);
    }
    return Future.value(uploadTask);
  }

  Future<String> retrieveFile() async {
    Reference ref =
        _firebaseStorage.ref().child('${_userFirestore.getUserId()}.jpg');

    if (ref != null) {
      return ref.getDownloadURL();
    } else {
      return "";
    }
  }
}
