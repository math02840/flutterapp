import 'package:firebase_auth/firebase_auth.dart';

import '../dataSources/remote/auth_firebase.dart';

class AuthRepository {
  static AuthRepository? _instance;
  static final AuthFirebase _userFirestore = AuthFirebase.getInstance();

  static AuthRepository getInstance() {
    _instance ??= AuthRepository._();
    return _instance!;
  }

  AuthRepository._();

  Future<User?> signIn(
      {required String email, required String password}) async {
    UserCredential userCredential = await _userFirestore.signInWithCredentials(
      email: email,
      password: password,
    );
    return userCredential.user;
  }

  Future<User?> signUp(
      {required String email, required String password}) async {
    UserCredential? userCredential = await _userFirestore.signUp(
      email: email,
      password: password,
    );
    return userCredential?.user;
  }

  Future<User?> getAuthenticatedUser() async {
    return _userFirestore.getSignedIn();
  }

  Future<void> signOut() async {
    return _userFirestore.signOut();
  }
}
