import 'package:flutter_application_1/data/entities/Results.dart';

import '../dataSources/remote/question_api.dart';
import '../entities/Question.dart';

class QuestionRepository {
  static QuestionRepository? _instance;

  static getInstance() {
    _instance ??= QuestionRepository._();
    return _instance;
  }

  QuestionRepository._();

  final QuestionApi _questionApi = QuestionApi.getInstance();

  Future<List<Question>> getFilteredQuestions() async {
    List<Question> list = await _questionApi.getQuestionsOfTheDay();

    Results results = Results(results: list, date: _getDate());
    return list;
  }

  String _getDate() {
    DateTime today = DateTime.now();
    return '${today.day}/${today.month}/${today.year}';
  }

  Future<List<Question>> getQuestionsOfTheDay() async {
    List<Question> list = await _questionApi.getQuestionsOfTheDay();

    Results results = Results(results: list, date: _getDate());

    if (results.date == _getDate()) {
      return results.results!;
    } else {
      List<Question> questions = await getFilteredQuestions();
      Results results = Results(results: questions, date: _getDate());

      return questions;
    }
  }
}
