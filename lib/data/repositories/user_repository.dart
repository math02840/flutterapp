import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_application_1/data/dataSources/remote/user_firebase.dart';
import 'package:image_picker/image_picker.dart';

import '../entities/User.dart';

class UserRepository {
  static UserRepository? _instance;
  static final UserFirebase _userFirestore = UserFirebase.getInstance();

  late var _connectedUser;

  static UserRepository getInstance() {
    _instance ??= UserRepository._();
    return _instance!;
  }

  UserRepository._();

  Future<TriviaUser?> getUserById(String id) async {
    TriviaUser? user = await _userFirestore.getUserById(id);
    _connectedUser = user!;
    return user;
  }

  Future<void> createUser(TriviaUser user, String id) async {
    _connectedUser = user;
    return await _userFirestore.insertUserWithId(user, id);
  }

  Future<void> uploadAvatar(XFile file) async {
    await _userFirestore.uploadFile(file);
  }

  Future<String> retrieveImage() async {
    String fileUrl = await _userFirestore.retrieveFile();
    return fileUrl;
  }

  TriviaUser getConnectedUser() {
    return _connectedUser;
  }

  Future<void> updateUser(TriviaUser user) async {
    _connectedUser = user;
    await _userFirestore.updateUser(user);
  }

  Future<List<TriviaUser>> getAllUsers() async {
    List<TriviaUser> users = await _userFirestore.getUsers();
    return users;
  }

  void signOut() {
    _connectedUser = null;
  }
}
