class TriviaUser {
   String? id;
   int? score;
   String? pseudo;
   String? avatar;
   int? game;

   TriviaUser({this.id, this.score, this.pseudo, this.avatar, this.game});

  @override
  String toString() {
    return 'User(id: $id, score: $score, pseudo: $pseudo, avatar: $avatar, game: $game)';
  }

  factory TriviaUser.fromMap(Map<String, dynamic> data) => TriviaUser(
        id: data['id'] as String?,
        score: data['score'] as int?,
        pseudo: data['pseudo'] as String?,
        avatar: data['avatar'] as String?,
        game: data['game'] as int?,
      );

  Map<String, dynamic> toMap() => {
        'id': id,
        'score': score,
        'pseudo': pseudo,
        'avatar': avatar,
        'game': game,
      };

  TriviaUser.fromJson(Map<String, dynamic> json) {
    id = json["id"];
    score = json["score"];
    pseudo = json["pseudo"];
    avatar = json["avatar"];
    game = json["game"];
  }

  /// `dart:convert`
  ///
  /// Converts [User] to a JSON string.
  Map<String, dynamic> toJson() {
    final map = <String, dynamic>{};
    map['id'] = id;
    map['score'] = score;
    map['pseudo'] = pseudo;
    map['avatar'] = avatar;
    map['game'] = game;
    return map;
  }

  TriviaUser copyWith({
    String? id,
    int? score,
    String? pseudo,
    String? avatar,
    int? game,
  }) {
    return TriviaUser(
      id: id ?? this.id,
      score: score ?? this.score,
      pseudo: pseudo ?? this.pseudo,
      avatar: avatar ?? this.avatar,
      game: game ?? this.game,
    );
  }
}
